class PostTitle < Liquid::Tag
  def initialize(tag_name, post, tokens)
    super
    @orig_post = post.strip
    begin
      @post = Jekyll::Tags::PostComparer.new(@orig_post)
    rescue => e
      raise Jekyll::Errors::PostURLError, <<-eos
Could not parse name of post "#{@orig_post}" in tag 'post_title'.
Make sure the post exists and the name is correct.
#{e.class}: #{e.message}
eos
    end
  end

  def render(context)
    site = context.registers[:site]

    site.posts.docs.each do |p|
      return p.data["title"] if @post == p
    end

    raise Jekyll::Errors::PostURLError, <<-eos
Could not find post "#{@orig_post}" in tag 'post_title'.
Make sure the post exists and the name is correct.
eos
  end
end

Liquid::Template.register_tag('post_title', PostTitle)
